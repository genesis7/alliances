# For partners requesting a GitLab EE licnese

>**NOTE**: Please make sure this issue is marked confidential when submitted. 

<!---
Please read this!
If you are submitting an issue to get your app listed on the Technology Partners page,  please include:

* Company Name
* Point of Contact Name and Email 
* Check the requirements that have been completed below or that apply to your request 
* Please comment and tag @abrandenburg in this issue
--->

| Contact Information | Details |
| ------ | ------ |
| Company Name | required |
| Point of Contact (First and Last) | required | 
| Email | required |
| Number of users | Up to 10


For GitLab Partner Manager: [license.gitlab.com](https://license.gitlab.com)

---
### Do Not Edit Below
/label  ~"Alliances" 
/label ~"Alliances - Ecosystem"
/label ~"Alliances - New Partner Request"
/assign @abrandenburg
/confidential
