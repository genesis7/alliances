#  Requesting a Partner dedicated [Subgroup](https://gitlab.com/gitlab-com/alliances)

<!---
Please read this!
If you are submitting an issue to provision a new partner subgroup please do the following:

* Submit this issue as a private issue
* Please make sure all GitLab user profiles created that will be granted access to the subgroup are registered with you company's email domain. Gmail and other non partner domains will not be granted access to the subgroup   
* Your subgroup will be private at first until all README.MD in the project content is filled out and ready for public consumption. 
* Please comment on this issue once the subgroup has been populated to make it a public project. 

--->

| Contact Information | Details |
| ------ | ------ |
| Company Name | required |
| Point of Contact (First and Last) | required -- Linked to GitLab.com accoount | 
| Email | required |
| Additional folks that require access | Partner official email associated with GitLab.com account


### Checklist for Partner Manager to create a new Subgroup

>**For Internal Reference**: See this [Subgroup example](https://gitlab.com/gitlab-com/alliances/ecosystem-partners-boilerplate). 

* [ ] *Partner Manager* - Create Sub-Group '*Partner Name*' under [Alliances](https://gitlab.com/gitlab-com/alliances/) and associated company logo. **Make sure the Subgroup is private.**
* [ ] *Partner Manager* - Create a new Project within '*Partner Name*' Subgroup titled with the solution name and a project slug `public-tracker`. include partner company/solution logo to the project. **Make sure the project is private.**
* [ ] *Partner Manager* -  Add the [ReadMe.MD](https://gitlab.com/gitlab-com/alliances/ecosystem-partners-boilerplate/public-tracker/blob/master/README.md) file template to the new repository. 
* [ ] *Partner Manager* - Create Subgroup '*Sandbox Projects*' under' *Partner Name*' Subgroup and associated [GitLab logo](https://logodix.com/logo/426432.png). Assign the following URL path `sandbox-projects`. **Make sure the Subgroup is private.**
* [ ] *Partner Manager* - Grant partner points of contact listed above in this issue description **Via the email's provided with @partner.com domain respectively** as *Developer* role to the new `public-tracker` project as well as the `sandbox-projects` Subgroup. 
* [ ] *Partner* Fill out the `README.MD` file in `public-tracker` project with additional information 
* [ ] *Partner* Create a new project for R&D, Demo, or other co-innovation purposes under `sandbox-projects` Subgroup to populate content into your sandbox. 
* [ ] *Partner* Comment on this issue tagging the *Partner Manager* if you want to make your `public-tracker` and/or `sandbox-projects* public and accessible by external users. 

>**NOTE FOR PARTNER MANAGER:** Do not grant access to partner points of contact directly to the top level `*Partner Name*` Sub-Group. Always grant access individually to `public-tracker` and `sandbox-projects` for partners via their official partner email addresses that should be associated with their new GitLab Account.

---
### Do Not Edit Below
/label  ~"Alliances" 
/label ~"Alliances - Ecosystem"
/assign @abrandenburg
/confidential
