For partners to create and request pre-sales support for a potential joint oppertunity. 

> **NOTE!!!** Please make sure when submitting this issue it is marked *Confidential*. This information will only be shared with GitLabbers and not be accessible to the public once submitted.

<!---
Please read this! PLEASE CREATE THIS ISSUE AS CONFIDENTIAL!

If you are submitting an issue to be aligned with our Feild Sales teams, please provide the following information:
--->

| Contact Information | Details |
| ------ | ------ |
| Customer Name | required |
| Partner Point of Contact (First and Last) | required | 
| Email | required | 

### Joint Oppertunity Details 

_Qualify the oppertunity with GitLab / Partner_
_Relative size of the oppertunity (user base? BU?)_
_Are they already a customer of yours? Looking to expand into an account with GitLab?_ 

### Ask
_How can we help? Need technical assistance?_
_Are you starting a Demo/POC with the customer regarding GitLab?_  
_Whare are suggested next steps with GitLab?_

### Questions or Comments 
_Notes/comments_

---
### Do Not Edit Below
/label  ~"Alliances" 
/label ~"Alliances - Ecosystem"
/label ~"Alliances - Sales || GTM"
/assign @abrandenburg
/confidential
