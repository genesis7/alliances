# For partners to looking to promote their joint solution on our [GitLab's UnFiltered YouTube Channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A). 

<!---
Please read this!
If you are submitting an issue to get your app listed on the Technology Partners page,  please include:

* Company Name
* Point of Contact Name and Email 
* Check the requirements that have been completed below or that apply to your request 
* Please comment and tag @abrandenburg in this issue

--->

| Contact Information | Details |
| ------ | ------ |
| Company Name | required |
| Point of Contact (First and Last) | required | 
| Email | required |

### Highlights to showcase during AMA
_Notes/comments_

### Questions or Comments 
_Notes/comments_

### Checklist for YouTube AMA on GitLab UnFiltered

* [ ] Integration is completed 
* [ ] Partner solution is listed in GitLab Partner's page
* [ ] Alliance Manager assigned to host AMA 
* [ ] Calendar invite sent with Zoom Link for meeting well in advance 
* [ ] Zoom call recorded and stored in Alliance' Shared G Drive
* [ ] Partner Marketing Reviews recording
* [ ] Alliance Manager uploads video to YouTube GitLab UnFiltered

---
### Do Not Edit Below
/label  ~"Alliances" 
/label ~"Alliances - Ecosystem"
/label ~"Alliances - New Partner Request"
/label ~"Alliances - Partner Marketing"
/assign @abrandenburg @Mayanktahil
/confidential
