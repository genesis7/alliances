Thank you for your interest as a partner with GitLab - we are glad you're here.

The initial step in exploring a potential partnership with GitLab is twofold: determine *how GitLab will interface with your product* and *define the business relationship* between GitLab and your company.
GitLab is intentional about documenting in a manner that creates a [single source of truth](https://about.gitlab.com/handbook/values/#single-source-of-truth). We value [asynchronous communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication) and transparency with our partners.

The Alliances team manages new partner requests and will review the issue. Generally, you will receive a response from GitLab within two weeks of submitting a new partner issue. This will either be an update on the status or a request for additional information. 

## Partnership Overview
Please include details about your company, the technical integration envisioned, end-user benefits and business relationship that you would ideally want. Our team will review and follow up with next steps. 

1. **Tell us more about your company. What do you do, how many employees, when were you founded, etc?** 

2. **What does the integration do? How do the two products work together for our joint customers?** 

3. **Is the integration/workflow with GitLab already completed?** 
* [ ]  Yes
* [ ]  No

4. **Do you use GitLab for your own internal development? (Optional)**
It's helpful to learn more about your developer toolstack and what the ecosystem is using so that we can support our partners and customers better. 
* [ ]  Yes
* [ ]  No

## GitLab Stages - Integration fit 

**What GitLab product stage does your company align with most?**  (https://about.gitlab.com/direction/maturity/) 
* [ ] Manage
* [ ] Plan 
* [ ] Create
* [ ] Verify
* [ ] Package
* [ ] Secure (Please check additional category below)
* [ ] Release 
* [ ] Configure 
* [ ] Monitor 
* [ ] Defend (Please check additional category below)

**Secure and Defend Product Category fit**

If you are a Secure or Defend partner, which product category do you align with most? For more information, please visit the [Secure Partner Integration - Onboarding Process page](https://docs.gitlab.com/ee/development/integrations/secure_partner_integration.html). 
* [ ] SAST
* [ ] DAST
* [ ] Fuzz Testing
* [ ] Dependency Scanning
* [ ] Container Scanning
* [ ] License Compliance 
* [ ] Secret Detection
* [ ] PKI Management 
* [ ] WAF
* [ ] Container Behavior Analytics
* [ ] Vulnerability Management
* [ ] Container Network Security
* [ ] UEBA
* [ ] Other (please provide a category if you select Other)

## Requirements to be an Ecosystem Partner
Which of the following steps have you completed? 
> Note: **New ecosystem partners filling out this issue**, it is not a requirement to have all of these completed before submitting your first issue. This will be used when we are ready for [Step 2](https://about.gitlab.com/partners/integrate/#-step-2-create-tech-docs-messaging-identify-customers-etc) discussions. 

1. * [ ]  Completed integration and I am ready to show a demo 
1. * [ ]  Documented publicly available information on the integration (please provide a link)
1. * [ ]  Identified mutual customer(s)
1. * [ ]  Submitted a merge request to be listed on the [GitLab Partners Page](https://about.gitlab.com/partners). Follow these [instructions on how to get your app listed](https://about.gitlab.com/handbook/alliances/integration-instructions/).

## Additional Requests
* [ ]  I would like to request a GitLab EE Ultimate license to develop and test our integration 
* [ ]  I would like to request a dedicated project under the Alliance Group and/or GitLab.com sandbox project 
* [ ]  I have a marketing request or go-to-market idea
* [ ]  I have a Public Relations (PR)/ External Communications Request
* [ ]  Other - please provide additional information

Once you have completed this issue, make sure to assign the issue or mention @abrandenburg in the comments 

>**What if I am having an issue with building my integration?**
We're always here to help you through your efforts of integration. If there's a missing API call from our current API, or you ran into other difficulties in your development please feel free to create a new issue on the [Community Edition issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/) and apply the `Ecosystem` label.


---
### Do Not Edit Below
/label  ~"Alliances" 
/label ~"Alliances - Ecosystem"
/label ~"Alliances - New Partner Request"
/assign @abrandenburg
